var express = require("express");
var router = express.Router();
const { User, UserVerify } = require("../models");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const saltRounds = 4;
const nodemailer = require("nodemailer");
const { v4: uuidv4 } = require("uuid");
const { Op } = require("sequelize");
const jwtAuthentication = require("../middlewares/jwtAuthentication");
/* GET users listing. */

router.post("/login", async (req, res, next) => {
  try {
    const { email, password } = req.body;
    // cari apakah ada user dengan email yang dimasukan
    const isUserFound = await User.findOne({
      where: {
        email,
      },
    });
    // kalo user gak ketemu kirim error
    if (!isUserFound) {
      return res.status(401).json({
        message: "email or password wrong",
      });
    }
    const isPasswordMatch = await bcrypt.compare(
      password,
      isUserFound.password
    );

    if (!isPasswordMatch) {
      return res.status(401).json({
        message: "email or password wrong",
      });
    }

    const token = await jwt.sign({ id: isUserFound.id }, "private");

    return res.json({
      message: "Succesfully login",
      token,
    });
  } catch (error) {
    console.log("error", error);
    res.status(401).json({
      message: error.message,
    });
  }
});

router.get("/verify/resend-email", jwtAuthentication, async (req, res) => {
  try {
    const user = await User.findOne({
      where: {
        id: req.user.id,
        verifiedAt: null,
      },
    });
    if (user) {
      const token = uuidv4();
      await UserVerify.create({
        token,
        email: user.email,
        userId: user.id,
        expiredAt: getExpiredDays(1),
      });
      return res.json({
        message: "Succesfully send verification email",
      });
    } else {
      return res.status(401).json({
        message: "Ada kesalahan di sisi user",
      });
    }
  } catch (error) {
    console.log("error", error);
    res.json({
      error,
    });
  }
});
router.get("/verify/:token", async (req, res) => {
  try {
    const validateToken = await UserVerify.findOne({
      where: {
        token: req.params.token,
        expiredAt: {
          [Op.gt]: new Date(),
        },
      },
    });
    console.log("validate token", validateToken);
    if (!validateToken) {
      return res.status(400).json({
        message: "Token verify invalid",
      });
    }
    const user = await User.findOne({
      where: {
        id: validateToken.userId,
      },
    });
    if (user.verifiedAt) {
      return res.status(400).json({
        message: "User already been verified",
      });
    }

    user.verifiedAt = new Date();
    user.save();

    return res.json({
      message: "Succesfully verified Email",
    });
  } catch (error) {
    console.log("error", error);
    res.json({
      error,
    });
  }
});

router.post("/register", async (req, res) => {
  const { email, password } = req.body;
  try {
    const isEmailExist = await User.findOne({
      where: {
        email,
      },
    });
    if (isEmailExist) {
      return res.status(401).json({
        message: "email already exist",
      });
    }

    const passwordHashed = await bcrypt.hash(password, saltRounds);

    const userCreated = await User.create({
      email,
      password: passwordHashed,
    });
    sendEmail(userCreated);
    return res.json({
      message: "Successfully create new user. Please Login",
    });
  } catch (error) {
    console.log("error", error);
    res.status(401).json({
      message: error.message,
    });
  }
});

const getExpiredDays = function (days) {
  var date = new Date();
  date.setDate(date.getDate() + days);
  return date;
};

const sendEmail = async (user) => {
  try {
    let testAccount = await nodemailer.createTestAccount();

    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
      host: "smtp.ethereal.email",
      port: 587,
      secure: false, // true for 465, false for other ports
      auth: {
        user: testAccount.user, // generated ethereal user
        pass: testAccount.pass, // generated ethereal password
      },
    });
    const findVerify = await UserVerify.findOne({
      where: {
        email: user.email,
      },
    });
    const token = uuidv4();

    if (findVerify) {
      return null;
    } else {
      await UserVerify.create({
        token,
        email: user.email,
        userId: user.id,
        expiredAt: getExpiredDays(1),
      });
    }

    // send mail with defined transport object
    let info = await transporter.sendMail({
      from: '"Fred Foo 👻" <foo@example.com>', // sender address
      to: user?.email, // list of receivers
      subject: "Hello ✔", // Subject line
      text: "Hello world?", // plain text body
      html: `
      <h1>Hello, ${user.email}</h1> <br>
      <div>this is your confirmation email</div>
      <div>please click this link below</div>
      <a href="http://localhost:3000/auth/verify/${token}" target="_blank">Verify Your Emaik</a>
      `, // html body
    });

    console.log("Message sent: %s", info.messageId);
    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

    // Preview only available when sending through an Ethereal account
    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  } catch (error) {
    console.log("error", error);
  }
};

module.exports = router;
