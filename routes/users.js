var express = require("express");
var router = express.Router();
const jwtAuthentication = require("../middlewares/jwtAuthentication");
const { UserGameHistory } = require("../models");
const { Op } = require("sequelize");
/* GET users listing. */
router.get("/", function (req, res, next) {
  res.send("respond with a resource");
});

// 1. userGame history join ke=> room by roomId => dari room join lagi ke UserGameHistory
// 1.5 detik
// 2. tinggal get lagi aja berdasarkan roomId, kemudian wherenya itu yang userId nya bukan kita
// 2 detik

router.get("/history", jwtAuthentication, async (req, res) => {
  try {
    // ini makan waktu 1 detik
    const UserGameHistories = await UserGameHistory.findAll({
      where: {
        userId: req.user.id,
      },
    });
    console.log("UserGameHistories", UserGameHistories[0].roomId);
    // makan waktu 1 detik
    const UserGameHistoriesEnemy = await UserGameHistory.findAll({
      where: {
        userId: {
          [Op.ne]: req.user.id,
        },
        roomId: UserGameHistories[0].roomId,
      },
    });
    res.json({
      UserGameHistories,
      UserGameHistoriesEnemy,
    });
  } catch (error) {
    res.json({
      message: error.message,
    });
  }
});

module.exports = router;
