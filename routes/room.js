var express = require("express");
var router = express.Router();
const { PlayerRoom, UserGameHistory } = require("../models");
const jwtAuthentication = require("../middlewares/jwtAuthentication");
const {
  createRoomController,
  joinRoomController,
} = require("../controllers/roomController");

const characters = "abcdefghijklmnopqrstuvwxyz0123456789";
function generateResult(yourChoice, enemyChoice) {
  if (yourChoice == enemyChoice) {
    return "draw";
  }
  if (yourChoice === "paper") {
    if (enemyChoice == "scissor") {
      return "lose";
    }
    if (enemyChoice == "rock") {
      return "win";
    }
  }
  if (yourChoice === "rock") {
    if (enemyChoice == "paper") {
      return "lose";
    }
    if (enemyChoice == "scissor") {
      return "win";
    }
  }
  if (yourChoice === "scissor") {
    if (enemyChoice == "rock") {
      return "lose";
    }
    if (enemyChoice == "paper") {
      return "win";
    }
  }
}

function getReverseResult(result) {
  if (result == "win") return "lose";
  if (result == "lose") return "win";
  return "draw";
}

// silahkan cari logic buat bikin gamenya beronde ronde.

const getGameRound = (count) => {
  if (count > 3) {
    return 3;
  }
  if (count > 1) {
    return 2;
  }

  return 1;
};
router.post("/fight/:idRoom", jwtAuthentication, async (req, res) => {
  try {
    const isRoomExist = await PlayerRoom.findOne({
      where: {
        uniqueName: req.params.idRoom,
      },
    });
    if (!isRoomExist) {
      return res.status(401).json({
        message: "Room not exist",
      });
    }

    const countHistoryGame = await UserGameHistory.count({
      roomId: isRoomExist.id,
    });
    if (countHistoryGame >= 6) {
      return res.status(400).json({
        message: "Session room already over",
      });
    }
    const isHistoryExist = await UserGameHistory.findOne({
      where: {
        roomId: isRoomExist.id,
      },
      order: [["createdAt", "DESC"]],
    });

    if (isHistoryExist) {
      if (
        isHistoryExist.userId == req.user.id &&
        getGameRound(countHistoryGame) == isHistoryExist.gameRound
      ) {
        return res.json({
          message: "you cant suit again",
        });
      } else {
        console.log(
          " getGameRound(countHistoryGame)",
          getGameRound(countHistoryGame)
        );

        const optionsCreate = {
          playerChoice: req.body.playerChoice,
          roomId: isRoomExist.id,
          userId: req.user.id,
          result: generateResult(
            req.body.playerChoice,
            isHistoryExist.playerChoice
          ),
          gameRound: getGameRound(countHistoryGame),
        };
        if (isHistoryExist.gameRound !== getGameRound(countHistoryGame)) {
          optionsCreate.result = null;
        }
        await UserGameHistory.create(optionsCreate);
        isHistoryExist.result = getReverseResult(
          generateResult(req.body.playerChoice, isHistoryExist.playerChoice)
        );
        // baru berubah jadi 6
        await isHistoryExist.save();
      }
    } else {
      await UserGameHistory.create({
        playerChoice: req.body.playerChoice,
        roomId: isRoomExist.id,
        userId: req.user.id,
        gameRound: 1,
      });

      // setTimeout(() => {

      // jumlah datanya ganjil atau genap
      // kalo ganjil berarti salah satu belom milih

      // ngecheck apakah player 2/1 (yang terakhir milih) udah pilih apa belom
      // kalo misalnya player 2/1 belom milih, ini otomatis kita buat random si player terakhirnya

      // player 1 udah milih
      // nunggu 20 detik
      // player 2 belom milih juga
      // player 2 pilihannya akan di random
      // disin akan selesai game round 1. dimana nanti player 1 milih apa player 2 milih apa, nanti resultnya apa

      // ketika player 2 milih lagi, nah itu gak bisa lanjutin rounde sebelumnya..
      // atau misal kan udah dirandomin, otomatis gamenya udah masuk ronde selanjutnya

      // nah ketika player 2 milih ini, langsung otomatis ngepilih buat round selanjutnya.
      // player 2 akan ngisi untuk game round 2. Nah disini player akan dikasih waktu 20 detik buat milih juga,
      // kalo player 1 gak milih janken juga, yaudah di random sama kaya player 2 sebelumnya.
      // }, 20000)
    }
    if (countHistoryGame >= 5) {
      const updateRoom = await PlayerRoom.update(
        {
          roomStatus: false,
        },
        {
          where: {
            id: isRoomExist.id,
          },
        }
      );
    }
    return res.json({
      message: "Selesai Fight",
    });
  } catch (error) {
    console.log("error", error);
    res.json({
      message: error.message,
    });
  }
});

router.post("/join/:idRoom", jwtAuthentication, joinRoomController);

router.post("/create", jwtAuthentication, createRoomController);

module.exports = router;
