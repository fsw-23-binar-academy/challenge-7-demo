const roomRepository = require("../repositories/roomRepository");

const characters =
  "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

const createRoomService = async (req) => {
  let err = null;
  try {
    const payload = {
      name: req.body.name,
      ownerId: req.user.id,
      uniqueName: generateString(5),
    };
    const [errFindOne, isPlayerHaveRoom] =
      await roomRepository.findOneRoomRepository({
        where: {
          ownerId: req.user.id,
          roomStatus: true,
        },
      });
    if (errFindOne) {
      return [errFindOne, null];
    }
    if (isPlayerHaveRoom) {
      err = {
        message: "You have already your own room",
      };
      return [err, isPlayerHaveRoom];
    }

    const [errCreated, createdRoom] = await roomRepository.createRoomRepository(
      payload
    );
    if (errCreated) {
      return [errCreated, createdRoom];
    }
    return [null, createdRoom];
  } catch (error) {
    return [error, null];
  }
};

const joinRoomService = async (req) => {
  let err = null;
  try {
    const options = {
      where: {
        uniqueName: req.params.idRoom,
      },
    };
    const [errorFindRoom, room] = await roomRepository.findOneRoomRepository(
      options
    );
    if (errorFindRoom) {
      return [errorFindRoom, null];
    }
    console.log("rom", room);
    if (!room) {
      err = {
        message: "Room not exist",
      };
    }
    if (room?.challengerId) {
      err = {
        message: "Room is full",
      };
    }
    if (room?.ownerId == req.user.id) {
      err = {
        message: "You cant join your own room",
      };
    }
    if (err) {
      return [err, null];
    }

    room.challengerId = req.user.id;
    room.save();

    return [err, room];
  } catch (error) {
    return [error, null];
  }
};

function generateString(length) {
  let result = "";
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }

  return result;
}

module.exports = { createRoomService, joinRoomService };
