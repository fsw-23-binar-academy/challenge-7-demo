const roomService = require("../services/roomService");

const createRoomController = async (req, res) => {
  const [err, response] = await roomService.createRoomService(req);
  if (err) {
    res.status(401).json({
      message: err.message,
    });
  } else {
    res.status(201).json({
      response,
    });
  }
};

const joinRoomController = async (req, res) => {
  const [err, response] = await roomService.joinRoomService(req);

  if (err) {
    res.status(401).json({
      message: err.message,
    });
  } else {
    res.status(201).json({
      response,
    });
  }
};

module.exports = { createRoomController, joinRoomController };
