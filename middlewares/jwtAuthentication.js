const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
  try {
    console.log("req", req.headers.authorization);

    if (!req?.headers?.authorization) {
      res.status(401).json({
        message: "Token Invalid",
      });
    }
    const token = req?.headers?.authorization.split(" ")[1];
    const isTokenValid = jwt.verify(token, "private");

    req.user = {
      id: isTokenValid.id,
    };
    next();
  } catch (error) {
    res.status(401).json({
      message: "Token Invalid",
    });
  }
};
