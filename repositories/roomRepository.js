const { PlayerRoom, UserGameHistory } = require("../models");

const findOneRoomRepository = async (options) => {
  let err = null;
  try {
    const room = await PlayerRoom.findOne(options);
    return [err, room];
  } catch (error) {
    return [error, null];
  }
};

const createRoomRepository = async (payload) => {
  try {
    const response = await PlayerRoom.create(payload);
    return [null, response];
  } catch (error) {
    return [error, null];
  }
};

module.exports = { createRoomRepository, findOneRoomRepository };
